document.addEventListener("DOMContentLoaded", function() {

  const addTodoInput = document.querySelector('.todo-input'),
      addTodoBtn = document.querySelector('.add-todo-btn'),
      completed = document.querySelector('.completed');

      addTodoBtn.addEventListener('click', function(){
        let inputValue = addTodoInput.value;
        if(inputValue !== '') newTask(inputValue);
      });

      document.querySelector('body').addEventListener('click', function(e) {
	       if(e.target.className === 'delete') e.target.parentElement.remove();
       });

       document.querySelector('body').addEventListener('click', function(e){
         if(e.target.className === 'done') completed.appendChild(e.target.parentElement);
       });

      function newTask(e) {
        let ul = document.querySelector('.tasks');
        let li = document.createElement('li');
        let span_done = document.createElement('span');
        let span_delete = document.createElement('span');
        let label_data = document.createElement('label');
        let children = ul.children.length + 1;
        span_done.setAttribute('class', 'done');
        span_done.appendChild(document.createTextNode('✔'));
        span_delete.setAttribute('class', 'delete');
        span_delete.appendChild(document.createTextNode('🗙'));
        label_data.setAttribute('class', 'data');
        li.setAttribute('class', 'task_'+children);
        label_data.appendChild(document.createTextNode(e));
        ul.appendChild(li);
        li.appendChild(label_data);
        li.appendChild(span_delete);
        li.appendChild(span_done);
      }
});
